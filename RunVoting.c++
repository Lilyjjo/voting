// -----------------------------------
// projects/c++/voting/RunVoting.c++
// Copyright (C) 2018
// Glenn P. Downing
// -----------------------------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Voting.h"

// ----
// main
// ----

int main () {
    using namespace std;
    voting_solve(cin, cout);
    return 0;
}
