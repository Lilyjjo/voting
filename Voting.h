// ------------------------------
// projects/c++/voting/Voting.h
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------

#ifndef Voting_h
#define Voting_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair
#include <vector>

using namespace std;

// ------------
// voting_eval
// ------------

/**
 * takes the candidate's current ballots and names and finds the winner
 * @param ballots vector of candidates, which are vectors of ballots, which are int vectors
 * @param names vector of the candidate names
 * @param candidateValidity vector representing the validity of the candidates
 * @param ballotsToWin the number of ballots a candidate needs to win
 * @return the names of the winner, each on their own line
 */
string voting_eval (vector <vector <vector <int>>>& ballots, vector<string>& names, vector<bool>& candidateValidity, int ballotsToWin);

// -------------
// voting_solve
// -------------

/**
 * @param r an istream
 * @param w an ostream
 */
void voting_solve (istream& r, ostream& w);

#endif // Voting_h
