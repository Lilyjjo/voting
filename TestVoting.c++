// ------------------------------------
// projects/c++/voting/TestVoting.c++
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Voting.h"

using namespace std;

// -----------
// TestVoting
// -----------

// ----
// eval
// ----



TEST(VotingFixture, eval_1) {

    vector<vector<vector<int>>> candidateArray = {{{3,2}},
        {{3,1},{2,1}},
        {}
    };
    vector<string> names = {"loser", "winner", "loser"};
    vector<bool> candidateValidity = {1,1,1};
    int ballotsToWin = 2;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "winner");
}

TEST(VotingFixture, eval_2) {

    vector<vector<vector<int>>> candidateArray = {{{3,2},{2,3},{3,2}},
        {{1,3},{3,1},{1,3}},
        {{1,2},{2,1},{1,2}}
    };
    vector<string> names = {"winner", "winner", "winner"};
    vector<bool> candidateValidity = {1,1,1};
    int ballotsToWin = 4;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "winner\nwinner\nwinner");
}

TEST(VotingFixture, eval_3) {

    vector<vector<vector<int>>> candidateArray = {{{4,3,2},{4,3,2}},
        {{1,2,4}},
        {{1,2,4}},
        {}
    };
    vector<string> names = {"thad", "chad", "brad", "luke"};
    vector<bool> candidateValidity = {1,1,1,1};
    int ballotsToWin = 3;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "thad");
}

TEST(VotingFixture, eval_4) {
    //two losers
    vector<vector<vector<int>>> candidateArray = {{{4,3,2}},
        {{1,3,4}},
        {{1,2,4}, {1,2,4}},
        {}
    };
    vector<string> names = {"thad", "chad", "brad", "luke"};
    vector<bool> candidateValidity = {1,1,1,1};
    int ballotsToWin = 3;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "brad");
}

TEST(VotingFixture, eval_5) {
    //two losers
    vector<vector<vector<int>>> candidateArray = {{{}}};
    vector<string> names = {"will"};
    vector<bool> candidateValidity = {1};
    int ballotsToWin = 1;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "will");
}

TEST(VotingFixture, eval_6) {
    //two losers
    vector<vector<vector<int>>> candidateArray = {{{3,2}},
        {{3,1}},
        {{1,2}}
    };
    vector<string> names = {"mert","anwesh","alberto"};
    vector<bool> candidateValidity = {1,1,1};
    int ballotsToWin = 2;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "mert\nanwesh\nalberto");
}

TEST(VotingFixture, eval_7) {
    //two losers
    vector<vector<vector<int>>> candidateArray = {{{3,2,4}},
        {{4,3,1},{4,3,1}},
        {{1,2,4}, {1,2,4}},
        {{2,1,3}}
    };
    vector<string> names = {"thad", "chad", "brad", "luke"};
    vector<bool> candidateValidity = {1,1,1,1};
    int ballotsToWin = 4;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "chad\nbrad");
}

TEST(VotingFixture, eval_8) {
    //two losers
    vector<vector<vector<int>>> candidateArray = {{{3,2,4}},
        {{4,3,1},{4,3,1}},
        {{1,2,4}, {1,2,4}},
        {}
    };
    vector<string> names = {"thad", "chad", "brad", "luke"};
    vector<bool> candidateValidity = {1,1,1,1};
    int ballotsToWin = 3;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "chad");
}

TEST(VotingFixture, eval_9) {
    //two losers
    vector<vector<vector<int>>> candidateArray = {{{3,2,4},{3,2,4},{3,2,4},{3,2,4},{3,2,4}},
        {{4,3,1},{4,3,1}},
        {{1,2,4}, {1,2,4},{1,3,2}},
        {}
    };
    vector<string> names = {"thad", "chad", "brad", "luke"};
    vector<bool> candidateValidity = {1,1,1,1};
    int ballotsToWin = 6;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "thad");
}

TEST(VotingFixture, eval_10) {
    //two losers
    vector<vector<vector<int>>> candidateArray = {{},
        {},
        {},
        {{3,2,4},{3,2,4},{3,2,4},{3,2,4},{3,2,4}}
    };
    vector<string> names = {"thad", "chad", "brad", "luke"};
    vector<bool> candidateValidity = {1,1,1,1};
    int ballotsToWin = 3;
    string v = voting_eval(candidateArray, names, candidateValidity, ballotsToWin);
    ASSERT_EQ(v, "luke");
}

// -----
// solve
// -----

TEST(VotingFixture, solve1) {
    istringstream r("1\n\n3\nmert\nanwesh\nalberto\n1 2 3\n3 2 1\n2 1 3\n");
    ostringstream w;
    voting_solve(r, w);
    ASSERT_EQ("mert\nanwesh\nalberto\n\n", w.str());
}

TEST(VotingFixture, solve2) {
    istringstream r("2\n\n3\nmert\nanwesh\nalberto\n1 2 3\n3 2 1\n2 1 3\n\n1\nwill\n1\n");
    ostringstream w;
    voting_solve(r, w);
    ASSERT_EQ("mert\nanwesh\nalberto\n\nwill\n\n", w.str());
}
