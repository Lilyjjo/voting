// --------------------------------
// projects/c++/voting/Voting.c++
// Copyright (C) 2018
// Glenn P. Downing
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <vector>

#include "Voting.h"

using namespace std;

// ------------
// voting_eval
// ------------

string voting_eval (vector <vector <vector <int>>>& ballots, vector<string>& names, vector<bool>& candidateValidity, int ballotsToWin) {
    assert(ballots.size() == names.size());
    assert(candidateValidity.size() == names.size());
    //check to see if majority
    //make list of people with the lowest number of ballots

    int validCandidates = ballots.size();

    while(validCandidates > 1) {
        int leastBallots = ballotsToWin;
        vector <int> losers;
        for(unsigned long i = 0; i < ballots.size(); i++) { //made i an unsigned long to compare with ballots.size(), which is an unsigned long
            if(candidateValidity[i]) {
                int numBallots = ballots[i].size();
                if(numBallots >= ballotsToWin) {
                    return names[i];
                }
                if (numBallots == leastBallots) {
                    losers.push_back(i);
                }
                if (numBallots < leastBallots) {
                    leastBallots = numBallots;
                    losers.clear();
                    losers.push_back(i);
                }
            }
        }
        //casted validCandidates to compare with unsigned long losers.size(), validCandidates will never be >20
        if(losers.size() == (unsigned long)validCandidates) {
            string output = "";
            for(int a : losers) {
                output += names[a] + "\n";
            }
            return output.substr(0, output.size() - 1);
        }
        for(unsigned long i = 0; i < losers.size(); i++) { //made i an unsigned long to compare with losers  .size(), which is an unsigned long
            validCandidates--;
            int loser = losers[i];
            candidateValidity[loser] = false;
            //find next candidate to give ballot to
            for(vector<int> ballot: ballots[loser]) {
                int vote = ballot[ballot.size()-1];
                while(!candidateValidity[vote-1]) {
                    ballot.pop_back();
                    vote = ballot[ballot.size() - 1];
                }
                ballot.pop_back();
                //have valid vote, used votes removed
                ballots[vote-1].emplace_back(ballot); //this might cause issue
            }
            ballots[loser].clear();
        }

    }
    for(unsigned long i = 0; i < candidateValidity.size(); ++i) { //made i an unsigned long to compare with candidateValidity.size(), which is an unsigned long
        if(candidateValidity[i]) {
            return names[i];
        }
    }
    return "";
}

// -------------
// voting_solve
// -------------

void voting_solve (istream& r, ostream& w) {
    string s;
    getline(r, s);
    istringstream sin(s);
    int cases;
    sin >> cases;

    getline(r, s);
    for(int iteration = 0; iteration < cases; iteration++) {
        getline(r, s);
        istringstream sin(s);
        int candidates;
        sin >> candidates;
        vector <string> names;
        vector <bool> candidateValidity(candidates);
        for(int c = 0; c < candidates; c++) {
            getline(r, s); //gets name of candidate
            names.push_back(s);
            candidateValidity[c] = true;
        }

        vector <vector <vector <int>>> ballots;
        for(int i = 0; i < candidates; i++) {
            //initializing candidate's vectors
            vector<vector<int>> candidate;
            ballots.push_back(candidate);
        }

        int numBallots = 0;
        while(getline(r, s) && !s.empty()) {
            ++numBallots;
            istringstream sin(s);
            vector <int> ballot(candidates);
            for(int i = candidates - 1; i >= 0; --i) {
                //builds ballot array backwards
                int cur;
                sin >> cur;
                ballot[i] = cur;
            }

            int choice = ballot[ballot.size()-1];
            ballot.pop_back();
            ballots[choice - 1].push_back(ballot); //pushing ballot into first choice candidate's vector
        }
        w << voting_eval(ballots, names, candidateValidity, (numBallots/2) + 1) << endl;
        w << endl;
    }
}
